# gitfeature

Automate github flow, creates feature pull requests, ensure code reviews and approvals, and merges feature branch into master.

# How to install
This tool is provided as a PyPi package. to install just run
```bash
pip install gitfeature
```

## How to use

1. Start working on your feature in a new branch
    ```bash
    git feature start feature-branch-name
    ```

2. When you are ready, create a Merge (Pull) Request, so that your team can review, request and approve the changes
    ```bash
    git feature review
    ```

3. When approved, merge the feature branch into master
    ```bash
    git feature deliver
    ```
